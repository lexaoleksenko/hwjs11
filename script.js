function showHidePasswordOne(target){
	let input = document.getElementById('password-input-one');
	if (input.getAttribute('type') == 'password') {
		target.classList.add('fa-eye-slash');
		input.setAttribute('type', 'text');
	} else {
		target.classList.remove('fa-eye-slash');
		input.setAttribute('type', 'password');
	}
	return false;
}

function showHidePasswordTwo(target){
	let input = document.getElementById('password-input-two');
	if (input.getAttribute('type') == 'password') {
		target.classList.add('fa-eye-slash');
		input.setAttribute('type', 'text');
	} else {
		target.classList.remove('fa-eye-slash');
		input.setAttribute('type', 'password');
	}
	return false;
}

function verifyPassword() {
	let passOne = document.getElementById('password-input-one').value;
	let passTwo = document.getElementById('password-input-two').value;
	let warning = document.querySelector('.warning')
	if(passOne == passTwo){
		alert('You are welcome!')
		warning.classList.remove('warningBlock')
	}  
	if(passOne != passTwo) {
		warning.classList.add('warningBlock')
	}

	
}
let verifyButton = document.querySelector('.btn')
verifyButton.addEventListener('click', verifyPassword)